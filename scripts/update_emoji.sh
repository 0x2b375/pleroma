#!/usr/bin/env bash

set -euo pipefail
shopt -s nullglob

DIR="$(set -e; git rev-parse --show-toplevel)"

main() {
  local config="${DIR}/config/emoji.txt"
  rm "${config}"

  pushd "${DIR}/config/emoji" >/dev/null
  for dir in *; do
    if [[ ! -d "${dir}" ]]; then
      continue
    fi

    pushd "${dir}" >/dev/null
    for img in *.png *.gif; do
      local code
      code="$(set -e; basename "${img}" .png)"
      code="$(set -e; basename "${code}" .gif)"

      echo "${code}, /emoji/custom/${dir}/${img}, ${dir}" >> "${config}"
    done
    popd >/dev/null
  done
  popd >/dev/null
}

main "$@"
