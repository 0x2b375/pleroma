#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "${PGUSER}" <<-EOSQL
ALTER ROLE ${POSTGRESQL_USER} SUPERUSER
EOSQL

