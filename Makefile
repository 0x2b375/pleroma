ROOT_PATH = "$(shell git rev-parse --show-toplevel)"

ADMIN_NAME ?= 0x2b375
ADMIN_EMAIL ?= 0x2b375@posteo.net

POSTGRESQL_DATABASE ?= pleroma
POSTGRESQL_USER ?= pleroma
POSTGRESQL_PASSWORD ?= pleroma
POSTGRESQL_ADMIN_PASSWORD ?= pleroma

.PHONY: pod
pod:
	@if ! podman pod exists pleroma_pod; then \
	  podman pod create --name pleroma_pod --publish 4000:4000; \
	fi

.PHONY: rm_pod
rm_pod:
	podman pod stop --ignore pleroma_pod
	podman pod rm --force --ignore pleroma_pod

.PHONY: pgsql_image
pgsql_image:
	@if ! podman image exists pleroma_postgres; then \
	  podman build --file Dockerfile.postgres --pull-always --tag pleroma_postgres .; \
	fi

.PHONY: rm_pgsql_image
rm_pgsql_image:
	@if podman image exists pleroma_postgres; then \
	  podman image rm --force pleroma_postgres; \
	fi

.PHONY: pgsql
pgsql: pod pgsql_image
	@if ! podman container exists pleroma_pgsql; then \
	  mkdir -p $(ROOT_PATH)/pgdata; \
	  podman create \
	    --pod pleroma_pod \
	    --name pleroma_pgsql \
	    --restart on-failure \
	    --volume $(ROOT_PATH)/pgdata:/var/lib/pgsql/data:Z \
	    --env POSTGRESQL_USER="$(POSTGRESQL_USER)" \
	    --env POSTGRESQL_PASSWORD="$(POSTGRESQL_PASSWORD)" \
	    --env POSTGRESQL_DATABASE="$(POSTGRESQL_DATABASE)" \
	    --env POSTGRESQL_ADMIN_PASSWORD="$(POSTGRESQL_ADMIN_PASSWORD)" \
	    pleroma_postgres; \
	fi

.PHONY: pleroma_image
pleroma_image:
	@if ! podman image exists pleroma; then \
	  podman build --file Dockerfile.pleroma --pull-always --tag pleroma .; \
	fi

.PHONY: rm_pleroma_image
rm_pleroma_image:
	@if podman image exists pleroma; then \
	  podman image rm --force pleroma; \
	fi

.PHONY: pleroma
pleroma: pod pleroma_image
	@if ! podman container exists pleroma_pleroma; then \
	  mkdir -p $(ROOT_PATH)/uploads; \
	  sudo chown 100665:100665 $(ROOT_PATH)/uploads; \
	  podman create \
	    --pod pleroma_pod \
	    --name pleroma_pleroma \
	    --restart on-failure \
	    --volume $(ROOT_PATH)/uploads:/home/pleroma/uploads:Z \
	    pleroma; \
	fi

.PHONY: containers
containers: pgsql pleroma

.PHONY: start
start: containers
	podman pod start pleroma_pod

.PHONY: stop
stop:
	podman pod stop pleroma_pod

.PHONY: gen_admin
gen_admin:
	echo Y | podman run --rm --interactive --pod pleroma_pod pleroma \
	  mix pleroma.user new "$(ADMIN_NAME)" "$(ADMIN_EMAIL)" --admin

.PHONY: gen_push_keys
gen_push_keys:
	podman run --rm pleroma mix web_push.gen.keypair

.PHONY: configure
configure: gen_admin gen_push_keys

.PHONY: clean
clean: rm_pod rm_pgsql_image rm_pleroma_image
