# Pleroma

This repository contains configuration files for my Pleroma instance at [0x2b375.moe](https://0x2b375.moe). It's hosted on the latest Fedora using rootless Podman.

## Usage

You need to configure rootless Podman, Docker is unsupported. Run the following:

```shell
# copy configuration template and change it (db password at least)
cp config/secret.exs.template config/secret.exs

# set required env variables
export ADMIN_NAME="<>"
export ADMIN_EMAIL="<>"
export POSTGRESQL_PASSWORD="password from the config"
export POSTGRESQL_ADMIN_PASSWORD="random strong password"

# create containers
make containers 

# create admin and web push creds
make configure

# start pleroma
make start
```

And then configure reverse-proxy using the `nginx.conf` Nginx configuration.
